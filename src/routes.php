<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes

// $app->get('/', function (Request $request, Response $response, array $args) {
//   // Sample log message
//   // $this->logger->info("Slim-Skeleton '/' route");
//
//   // Render index view
//   return $this->renderer->render($response, 'index.phtml', $args);
// });
$app->get(
  '/prodotti[/]',
  \Controller\ProdottiController::class.':getAll'
);

$app->get(
  '/prodotti/{id:[0-9]+}[/]',
  \Controller\ProdottiController::class.':get'
);

$app->get(
  '/ordini/{id:[0-9]+}[/]',
  \Controller\OrdiniController::class.':get'
);

$app->post(
  '/ordini[/]',
  \Controller\OrdiniController::class.':create'
);
