<?php

namespace Controller;

use Slim\Http\Request;
use Slim\Http\Response;
use \RedBeanPHP\R as DB;

class OrdiniController {
  protected $container;
  protected $logger;
  protected $spesaTrasporto = 2.00;

  public function __construct(\Slim\Container $container) {
    $this->container = $container;
    $this->logger = $container->get('logger');
  }

  public function get(Request $request, Response $response, array $args) {
    $ordine  = DB::findOne( 'ordini', $args['id']);

    if(!$ordine) {
      //Ritorno un errore 404
      return $response->withStatus(404);
    }

    $prodotti = DB::getAssocRow( 'SELECT * FROM riga_ordine INNER JOIN prodotti ON ro_ID_prodotto = pr_ID WHERE ro_ID_ordine = :ID',
      array(':ID' => $args['id']));
    $ordine["prodotti"] = $prodotti;

    return $response->withJson($ordine);
  }

  public function create(Request $request, Response $response, array $args) {
    $ret = [
      'status' => false,
      'message' => 'Errore. Riprovare piu\' tardi.',
      'id' => null
    ];

    $parametri = $request->getParsedBody();

    $parametri = ($parametri==null) ? [] : $parametri;

    // $ret['parametri'] = $parametri;

    // error_reporting(E_ALL & ~E_NOTICE);

    if(!array_key_exists('telefono', $parametri)
      || !$parametri['telefono']
      || !array_key_exists('email', $parametri)
      || !$parametri['email']
      || !array_key_exists('via', $parametri)
      || !$parametri['via']
      || !array_key_exists('numero-civico', $parametri)
      || !$parametri['numero-civico']
      || !array_key_exists('ora-consegna', $parametri)
      || !$parametri['ora-consegna']
      || !array_key_exists('citta', $parametri)
      || !$parametri['citta']
      || !array_key_exists('prodotti', $parametri)
      || !$parametri['prodotti'])
    {
      $ret['message'] = 'parametri non validi(1)';
      return $response->withStatus(400)->withJson($ret);
    }
    $importo = 0;
    $prodotti = [];
    foreach ($parametri['prodotti'] as $key => $p) {
      $quantita = intval($p["quantita"]);
      if ($quantita < 1){
        continue;
      }

      $ID = intval($p["id"]);
      $prodotto = $this->verificaProdototto($ID);
      if ($prodotto === null){
        continue;
      }

      array_push($prodotti, ['ID' => $ID, 'quantita' => $quantita]);
      $importo += floatval($prodotto["pr_prezzo"]) * $quantita;
    }

    if (!count($prodotti)) {
      $ret['message'] = 'parametri non validi(2)';
      return $response->withStatus(400)->withJson($ret);
    }

    $q_or = 'INSERT INTO ordini
      (or_telefono, or_email, or_via, or_numero_civico, or_ora_consegna, or_citta, or_importo, or_stato)
      VALUES (:telefono, :email, :via, :numero_civico, :ora_consegna, :citta, :importo, 1)';
    $q_ro = 'INSERT INTO riga_ordine
      (ro_ID_ordine, ro_ID_prodotto, ro_quantita)
      VALUES (:ID_ordine, :ID_prodotto, :quantita)';

    DB::begin();
    try{
      $v_or = [
        'telefono' => $parametri['telefono'],
        'email' => $parametri['email'],
        'via' => $parametri['via'],
        'numero_civico' => $parametri['numero-civico'],
        'ora_consegna' => $parametri['ora-consegna'],
        'citta' => $parametri['citta'],
        'importo' => $importo + $this-> spesaTrasporto
      ];
      DB::exec($q_or,  $v_or);
      $id = DB::getInsertID();

      if (!$id) {
        DB::rollback();
        return $response->withStatus(400)->withJson($ret);
      }

      $ret['id'] = $id;

      foreach ($prodotti as $key => $prodotto) {
        try{
          $v_ro = ['ID_ordine' => $id, 'ID_prodotto' => $prodotto['ID'], 'quantita' => $prodotto['quantita']];
          DB::exec($q_ro,  $v_ro);
        }
        catch( \Exception $e ) {
          $this->logger->error($e);
          DB::rollback();
        }
      }

      $ret['status'] = true;
      DB::commit();
    }
    catch( \Exception $e ) {
      $this->logger->error($e);
      DB::rollback();
    }
    $statusCode = ($ret['status']) ? 201 : 400 ;
    return $response->withStatus($statusCode)->withJson($ret);
  }

  public function verificaProdototto($ID) {

      $prodotto = DB::getRow( 'SELECT * FROM prodotti WHERE pr_eliminato = 0 AND pr_ID = ? LIMIT 1', array($ID));

      if(!$prodotto) {
          return null;
      } else {
          return $prodotto;
      }
  }
}
