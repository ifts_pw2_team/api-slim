<?php

namespace Controller;

use Slim\Http\Request;
use Slim\Http\Response;
use \RedBeanPHP\R as DB;

class ProdottiController {

    protected $container;
    protected $logger;

    public function __construct(\Slim\Container $container) {
        $this->container = $container;
        $this->logger = $container->get('logger');
    }

    /**
     * Ritorna in json tutti i prodotti
     *
     * @return void
     */
    public function getAll(Request $request, Response $response, array $args) {
        $prodotti = DB::getAll( 'SELECT * FROM prodotti WHERE pr_eliminato = 0');

        // $this->logger->info("prodotti: ".serialize($prodotti));
        return $response->withJson($prodotti);
    }
}
